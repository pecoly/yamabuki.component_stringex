﻿using System;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.StringEx
{
    internal partial class StSubString_V
        : DsEditForm_V, StSubString_VI
    {
        private EventHandler okayButton_Click;

        private EventHandler cancelButton_Click;

        public StSubString_V()
        {
            this.InitializeComponent();
        }

        public Action OkButton_Click
        {
            set
            {
                this.okButton.Click -= this.okayButton_Click;
                this.okayButton_Click = (sender, e) => value();
                this.okButton.Click += this.okayButton_Click;
            }
        }

        public Action CancelButton_Click
        {
            set
            {
                this.cancelButton.Click -= this.cancelButton_Click;
                this.cancelButton_Click = (sender, e) => value();
                this.cancelButton.Click += this.cancelButton_Click;
            }
        }

        public String Message
        {
            set { this.statusLabel.Text = value; }
        }

        public Boolean HasBeginIndex
        {
            get { return this.hasBeginIndexCheckButton.Checked; }
            set { this.hasBeginIndexCheckButton.Checked = value; }
        }

        public Boolean HasLength
        {
            get { return this.hasLengthCheckButton.Checked; }
            set { this.hasLengthCheckButton.Checked = value; }
        }

        public override void Initialize()
        {
            this.UpdateHasBeginIndexCheckButtonText();
            this.UpdateHasLengthCheckButtonText();

            this.hasBeginIndexCheckButton.CheckedChanged +=
                (sender, e) => this.UpdateHasBeginIndexCheckButtonText();

            this.hasLengthCheckButton.CheckedChanged +=
                (sender, e) => this.UpdateHasLengthCheckButtonText();
        }

        private void UpdateHasBeginIndexCheckButtonText()
        {
            if (this.hasBeginIndexCheckButton.Checked)
            {
                this.hasBeginIndexCheckButton.Text = "指定する";
            }
            else
            {
                this.hasBeginIndexCheckButton.Text = "指定しない";
            }
        }

        private void UpdateHasLengthCheckButtonText()
        {
            if (this.hasLengthCheckButton.Checked)
            {
                this.hasLengthCheckButton.Text = "指定する";
            }
            else
            {
                this.hasLengthCheckButton.Text = "指定しない";
            }
        }
    }
}
