﻿using System;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.StringEx
{
    internal class StSubString_P
        : DsEditForm_P<StSubString_VI, DsSubString>
    {
        public StSubString_P(DsSubString com)
            : base(com)
        {
        }

        public override void Load()
        {
            this.View.Initialize();
            this.View.HasBeginIndex = this.Component.HasBeginIndex;
            this.View.HasLength = this.Component.HasLength;
        }

        public override void Save()
        {
            this.Component.HasBeginIndex = this.View.HasBeginIndex;
            this.Component.HasLength = this.View.HasLength;
        }
    }
}
