﻿using System;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.StringEx
{
    internal interface StSubString_VI
        : DsEditForm_VI
    {
        Boolean HasBeginIndex { get; set; }

        Boolean HasLength { get; set; }
    }
}
