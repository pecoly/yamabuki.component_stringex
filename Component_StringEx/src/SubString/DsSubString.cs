﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

using Yamabuki.Component.StringEx.Properties;
using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.Message;
using Yamabuki.Design.SimpleEditForm;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Func;
using Yamabuki.Task.Utility;
using Yamabuki.Utility.Cast;
using Yamabuki.Window.Core;

namespace Yamabuki.Component.StringEx
{
    public class DsSubString
        : DsSimpleComponent_N_1
    {
        public override int DefaultWidth
        {
            get { return 144; }
        }

        internal Boolean HasBeginIndex { get; set; }

        internal Boolean HasLength { get; set; }
                
        public override void Initialize(IEnumerable<XElement> data)
        {
            foreach (var e in data)
            {
                if (e.Name == Property.HasBeginIndex)
                {
                    this.HasBeginIndex = StringToBoolean(e.Value);
                }
                else if (e.Name == Property.HasLength)
                {
                    this.HasLength = StringToBoolean(e.Value);
                }
            }
        }

        public override BaseMessage DoubleClick()
        {
            var oldHasBeginIndex = this.HasBeginIndex;
            var oldHasLength = this.HasLength;
            var oldE = this.GetXElement();
            var result = this.ShowDialog();

            var isUpdated = result == FormResult.Ok;
            if (!isUpdated)
            {
                return null;
            }

            var messageCollection = new MessageCollection();

            var index = 1;
            messageCollection.Add(this.UpdateInputMessage(oldHasBeginIndex, this.HasBeginIndex, index, "開始位置"));
            
            if (this.HasBeginIndex)
            {
                index++;
            }

            messageCollection.Add(this.UpdateInputMessage(oldHasLength, this.HasLength, index, "長さ"));            

            messageCollection.Add(new UpdateComponentMessage(oldE, this.GetXElement()));

            return messageCollection;
        }

        public override void SetParameter(IDictionary<String, String> paramList)
        {
            foreach (var kv in paramList)
            {
                if (kv.Key == Property.HasBeginIndex)
                {
                    this.HasBeginIndex = StringToBoolean(paramList[Property.HasBeginIndex]);
                }
                else if (kv.Key == Property.HasLength)
                {
                    this.HasLength = StringToBoolean(paramList[Property.HasLength]);
                }
            }
        }

        public override IEnumerable<XElement> DataToXElement()
        {
            var list = new List<XElement>();
            list.Add(new XElement(Property.HasBeginIndex, this.HasBeginIndex));
            list.Add(new XElement(Property.HasLength, this.HasLength));
            return list;
        }

        internal static Boolean StringToBoolean(String s)
        {
            return CastUtils.ToBoolean(s, (x) => false);
        }

        internal BaseMessage UpdateInputMessage(
            Boolean oldHasBeginIndex,
            Boolean newHasBeginIndex,
            Int32 index,
            String name)
        {
            if (!oldHasBeginIndex && newHasBeginIndex)
            {
                return this.InsertInputPort(index, name);
            }
            else if (oldHasBeginIndex && !newHasBeginIndex)
            {
                return this.RemoveInputPort(index);
            }

            return new NullMessage();
        }

        protected override void Initialize_N_1()
        {
            this.HasBeginIndex = false;
            this.HasLength = false;

            this.AddInputPort("データ");
        }

        protected virtual FormResult ShowDialog()
        {
            using (var presenter = new StSubString_P(this))
            using (var view = new StSubString_V())
            {
                presenter.View = view;
                return presenter.ShowModal();
            }
        }

        protected override TsTask GetTask(
            IEnumerable<TsDataStoreGuid> inputDataStoreGuidList,
            TsDataStoreGuid outputDataStoreGuid)
        {
            var inputList = inputDataStoreGuidList.ToList();

            if (!this.HasBeginIndex && !this.HasLength)
            {
                return new TsFuncTask_1_1__N_N<String, String>(
                    this.DefinitionPath,
                    inputList[0],
                    outputDataStoreGuid,
                    this.Function0);
            }
            else if (!this.HasBeginIndex && this.HasLength)
            {
                return new TsFuncTask_2_1__NN_N<String, Int32, String>(
                    this.DefinitionPath,
                    inputList[0],
                    inputList[1],
                    outputDataStoreGuid,
                    this.Function1);
            }
            else if (this.HasBeginIndex && !this.HasLength)
            {
                return new TsFuncTask_2_1__NN_N<String, Int32, String>(
                    this.DefinitionPath,
                    inputList[0],
                    inputList[1],
                    outputDataStoreGuid,
                    this.Function2);
            }
            else if (this.HasBeginIndex && this.HasLength)
            {
                return new TsFuncTask_3_1__NNN_N<String, Int32, Int32, String>(
                    this.DefinitionPath,
                    inputList[0],
                    inputList[1],
                    inputList[2],
                    outputDataStoreGuid,
                    this.Function3);
            }
            else
            {
                throw new Exception();
            }
        }

        private List<String> Function0(List<String> valueList)
        {
            return valueList.Select(y => y == null ? "" : y).ToList();
        }

        private List<String> Function1(List<String> valueList, List<Int32> lengthList)
        {
            var func = new Func<String, Int32, String>((value, length) =>
            {
                if (value == null)
                {
                    return "";
                }

                if (value.Length < length)
                {
                    return "";
                }

                return value.Substring(0, length);
            });

            try
            {
                var result = new List<String>();
                TsDataListUtilsT.Func_M_N_O<String, Int32, String>(valueList, lengthList, result, func);
                return result;
            }
            catch (TsFunctionException e)
            {
                switch (e.ErrorCode)
                {
                    case TsErrorCodeList.TsInputDataInvalidLength:
                        throw new TsInputDataInvalidLengthException(this.DefinitionPath, Resource.InvalidDataLength);
                    default:
                        throw new TsException(this.DefinitionPath, e.ErrorCode, Resource.UndefinedException);
                }
            }
        }

        private List<String> Function2(List<String> valueList, List<Int32> indexList)
        {
            var func = new Func<String, Int32, String>((value, beginIndex) =>
            {
                if (value == null)
                {
                    return "";
                }

                if (value.Length < beginIndex)
                {
                    return "";
                }

                return value.Substring(beginIndex);
            });

            try
            {
                var result = new List<String>();
                TsDataListUtilsT.Func_M_N_O<String, Int32, String>(valueList, indexList, result, func);
                return result;
            }
            catch (TsFunctionException e)
            {
                switch (e.ErrorCode)
                {
                    case TsErrorCodeList.TsInputDataInvalidLength:
                        throw new TsInputDataInvalidLengthException(this.DefinitionPath, Resource.InvalidDataLength);
                    default:
                        throw new TsException(this.DefinitionPath, e.ErrorCode, Resource.UndefinedException);
                }
            }
        }

        private List<String> Function3(List<String> valueList, List<Int32> indexList, List<Int32> lengthList)
        {
            var func = new Func<String, Int32, Int32, String>((value, beginIndex, length) =>
            {
                if (value == null)
                {
                    return "";
                }

                if (value.Length < beginIndex)
                {
                    return "";
                }

                if (value.Length < beginIndex + length)
                {
                    return value.Substring(beginIndex);
                }

                return value.Substring(beginIndex, length);
            });

            try
            {
                var result = new List<String>();
                TsDataListUtilsT.Func_M_N_O_P<String, Int32, Int32, String>(valueList, indexList, lengthList, result, func);
                return result;
            }
            catch (TsFunctionException e)
            {
                switch (e.ErrorCode)
                {
                    case TsErrorCodeList.TsInputDataInvalidLength:
                        throw new TsInputDataInvalidLengthException(this.DefinitionPath, Resource.InvalidDataLength);
                    default:
                        throw new TsException(this.DefinitionPath, e.ErrorCode, Resource.UndefinedException);
                }
            }
        }

        /// <summary>プロパティ</summary>
        private struct Property
        {
            internal const String HasBeginIndex = "HasBeginIndex";
            internal const String HasLength = "HasLength";
        }
    }
}
