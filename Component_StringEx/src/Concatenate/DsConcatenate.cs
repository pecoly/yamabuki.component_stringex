﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

using Yamabuki.Component.StringEx.Properties;
using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.SimpleEditForm;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Func;
using Yamabuki.Task.Utility;

namespace Yamabuki.Component.StringEx
{
    public class DsConcatenate
        : DsSimpleComponent_2_1
    {
        private const String Title = "連結";

        internal String Description
        {
            get
            {
                return "文字列を連結します。\r\n" +
                    "例 : \r\n" +
                    "入力1 : abc\r\n" +
                    "入力2 : def\r\n" +
                    "出力 : abcdef";
            }
        }

        public override void Initialize(IEnumerable<XElement> data)
        {
        }

        public override BaseMessage DoubleClick()
        {
            this.ShowDialog();
            return null;
        }

        protected override void Initialize_2_1()
        {
        }

        protected virtual void ShowDialog()
        {
            using (var presenter = new DsSimpleEditForm_P(this))
            {
                presenter.Show(FormSize.Auto, Title, this.Description);
            }
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuid0,
            TsDataStoreGuid inputDataStoreGuid1,
            TsDataStoreGuid outputDataStoreGuid)
        {
            var func = new Func<List<String>, List<String>, List<String>>((x, y) =>
            {
                try
                {
                    var z = new List<String>();
                    TsDataListUtilsT.Func_M_N_O<String, String, String>(x, y, z, (a, b) => (a == null ? "" : a) + (b == null ? "" : b));
                    return z;
                }
                catch (TsFunctionException e)
                {
                    switch (e.ErrorCode)
                    {
                        case TsErrorCodeList.TsInputDataInvalidLength:
                            throw new TsInputDataInvalidLengthException(this.DefinitionPath, Resource.InvalidDataLength);
                        default:
                            throw new TsException(this.DefinitionPath, e.ErrorCode, Resource.UndefinedException);
                    }
                }
            });

            return new TsFuncTask_2_1__NN_N<String, String, String>(
                this.DefinitionPath,
                inputDataStoreGuid0,
                inputDataStoreGuid1,
                outputDataStoreGuid,
                func);
        }
    }
}
