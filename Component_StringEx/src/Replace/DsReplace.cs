﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

using Yamabuki.Component.StringEx.Properties;
using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.SimpleEditForm;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Func;
using Yamabuki.Task.Utility;

namespace Yamabuki.Component.StringEx
{
    public class DsReplace
        : DsSimpleComponent_3_1
    {
        private const String Title = "置換";

        public override int DefaultWidth
        {
            get { return 144; }
        }

        internal String Description
        {
            get
            {
                return "文字列を置換します。\r\n" +
                    "例 : \r\n" +
                    "データ : グレープ、グレープフルーツ、オレンジ\r\n" +
                    "置換前 : グレープ\r\n" +
                    "置換後 : バナナ\r\n" +
                    "出力 : バナナ、バナナフルーツ、オレンジ";
            }
        }

        protected override string InputName0
        {
            get { return "データ"; }
        }

        protected override string InputName1
        {
            get { return "置換前"; }
        }

        protected override string InputName2
        {
            get { return "置換後"; }
        }

        public override void Initialize(IEnumerable<XElement> data)
        {
        }

        public override BaseMessage DoubleClick()
        {
            this.ShowDialog();
            return null;
        }

        protected override void Initialize_3_1()
        {
        }

        protected virtual void ShowDialog()
        {
            using (var presenter = new DsSimpleEditForm_P(this))
            {
                presenter.Show(FormSize.Auto, Title, this.Description);
            }
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuid0,
            TsDataStoreGuid inputDataStoreGuid1,
            TsDataStoreGuid inputDataStoreGuid2,
            TsDataStoreGuid outputDataStoreGuid)
        {
            return new TsFuncTask_3_1__NNN_N<String, String, String, String>(
                this.DefinitionPath,
                inputDataStoreGuid0,
                inputDataStoreGuid1,
                inputDataStoreGuid2,
                outputDataStoreGuid,
                this.Function);
        }

        private List<String> Function(List<String> valueList, List<String> srcList, List<String> dstList)
        {
            var func = new Func<String, String, String, String>((value, src, dst) =>
            {
                if (value == null)
                {
                    return "";
                }

                return value.Replace(src, dst);
            });

            try
            {
                var result = new List<String>();
                TsDataListUtilsT.Func_M_N_O_P<String, String, String, String>(valueList, srcList, dstList, result, func);
                return result;
            }
            catch (TsFunctionException e)
            {
                switch (e.ErrorCode)
                {
                    case TsErrorCodeList.TsInputDataInvalidLength:
                        throw new TsInputDataInvalidLengthException(this.DefinitionPath, Resource.InvalidDataLength);
                    default:
                        throw new TsException(this.DefinitionPath, e.ErrorCode, Resource.UndefinedException);
                }
            }
        }
    }
}
