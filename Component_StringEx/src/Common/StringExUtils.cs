﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

using Yamabuki.Utility.Cast;
using Yamabuki.Utility.Log;

namespace Yamabuki.Component.StringEx
{
    internal static class StringExUtils
    {
        /// <summary>ロガー</summary>
        private static readonly Logger Logger = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// String型をカラム数に変換します。
        /// 最小値は1です。
        /// 変換できないときは1を返却します。
        /// </summary>
        /// <param name="s">変換する文字列</param>
        /// <returns>変換した値</returns>
        internal static Int32 StringToInputLength(String s)
        {
            return Math.Max(StringToInt32(s), 1);
        }

        /// <summary>
        /// String型をInt32型に変換します。
        /// 変換できないときは0を返却します。
        /// </summary>
        /// <param name="s">変換する文字列</param>
        /// <returns>変換した値</returns>
        internal static Int32 StringToInt32(String s)
        {
            return CastUtils.ToInt32(s, (x) => 0);
        }
    }
}
