﻿using System;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.StringEx
{
    internal partial class StFormat_V
        : DsEditForm_V, StFormat_VI
    {
        private EventHandler okayButton_Click;

        private EventHandler cancelButton_Click;

        public StFormat_V()
        {
            this.InitializeComponent();
        }

        public Action OkButton_Click
        {
            set
            {
                this.okButton.Click -= this.okayButton_Click;
                this.okayButton_Click = (sender, e) => value();
                this.okButton.Click += this.okayButton_Click;
            }
        }

        public Action CancelButton_Click
        {
            set
            {
                this.cancelButton.Click -= this.cancelButton_Click;
                this.cancelButton_Click = (sender, e) => value();
                this.cancelButton.Click += this.cancelButton_Click;
            }
        }

        public String Message
        {
            set { this.statusLabel.Text = value; }
        }

        public Int32 InputLength
        {
            get { return Decimal.ToInt32(this.inputLengthUpDown.Value); }
            set { this.inputLengthUpDown.Value = value; }
        }

        public override void Initialize()
        {
            this.inputLengthUpDown.Focus();
        }
    }
}
