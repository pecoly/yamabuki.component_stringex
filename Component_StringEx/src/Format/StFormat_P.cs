﻿using System;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.StringEx
{
    internal class StFormat_P
        : DsEditForm_P<StFormat_VI, DsFormat>
    {
        public StFormat_P(DsFormat com)
            : base(com)
        {
        }

        public override void Load()
        {
            this.View.Initialize();
            this.View.InputLength = this.Component.InputLength;
        }

        public override void Save()
        {
            this.Component.InputLength = this.View.InputLength;
        }
    }
}
