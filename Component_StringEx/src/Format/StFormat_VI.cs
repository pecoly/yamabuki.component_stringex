﻿using System;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.StringEx
{
    internal interface StFormat_VI
        : DsEditForm_VI
    {
        Int32 InputLength { get; set; }
    }
}
