﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

using Yamabuki.Component.StringEx.Properties;
using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.Message;
using Yamabuki.Design.SimpleEditForm;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Func;
using Yamabuki.Task.Utility;
using Yamabuki.Utility.Cast;
using Yamabuki.Window.Core;

namespace Yamabuki.Component.StringEx
{
    public class DsFormat
        : DsSimpleComponent_N_1
    {
        private const String FormatName = "フォーマット";

        public override int DefaultWidth
        {
            get { return 144; }
        }

        internal Int32 InputLength { get; set; }
                
        public override void Initialize(IEnumerable<XElement> data)
        {
            foreach (var e in data)
            {
                this.InitializeParameter(e.Name.LocalName, e.Value);
            }
        }

        public override BaseMessage DoubleClick()
        {
            var oldE = this.GetXElement();
            var oldInputLength = this.InputLength;
            var result = this.ShowDialog();

            var isUpdated = result == FormResult.Ok;
            if (!isUpdated)
            {
                return null;
            }

            var messageCollection = new MessageCollection();

            foreach (var message in this.UpdateDataInputMessage(oldInputLength, this.InputLength, 1))
            {
                messageCollection.Add(message);
            }

            messageCollection.Add(new UpdateComponentMessage(oldE, this.GetXElement()));

            return messageCollection;
        }

        public override void SetParameter(IDictionary<String, String> paramList)
        {
            foreach (var kv in paramList)
            {
                this.InitializeParameter(kv.Key, kv.Value);
            }
        }

        public override IEnumerable<XElement> DataToXElement()
        {
            var list = new List<XElement>();
            list.Add(new XElement(Property.InputLength, this.InputLength));
            return list;
        }

        internal IEnumerable<BaseMessage> UpdateDataInputMessage(
            Int32 oldInputLength, Int32 newInputLength, Int32 indexOffset)
        {
            var messageList = new List<BaseMessage>();

            if (oldInputLength == newInputLength)
            {
                return messageList;
            }
            else if (oldInputLength < newInputLength)
            {
                // 追加されたとき
                for (var i = 0; i < newInputLength - oldInputLength; i++)
                {
                    messageList.Add(this.AddInputPort(""));
                }
            }
            else if (oldInputLength < newInputLength)
            {
                // 削除されたとき
                var index = oldInputLength - 1 + indexOffset;
                for (var i = 0; i < oldInputLength - newInputLength; i++)
                {
                    messageList.Add(this.RemoveInputPort(index));
                    index--;
                }
            }

            return messageList;
        }

        internal void InitializeParameter(String key, String value)
        {
            if (key == Property.InputLength)
            {
                this.InputLength = StringExUtils.StringToInputLength(value);
            }
        }
        
        protected override void Initialize_N_1()
        {
            this.InputLength = 1;

            this.AddInputPort(FormatName);
            this.AddInputPort("");
        }

        protected virtual FormResult ShowDialog()
        {
            using (var presenter = new StFormat_P(this))
            using (var view = new StFormat_V())
            {
                presenter.View = view;
                return presenter.ShowModal();
            }
        }

        protected override TsTask GetTask(
            IEnumerable<TsDataStoreGuid> inputDataStoreGuidList,
            TsDataStoreGuid outputDataStoreGuid)
        {
            return new TsFormat(
                this.DefinitionPath,
                inputDataStoreGuidList,
                outputDataStoreGuid);
        }

        /// <summary>プロパティ</summary>
        private struct Property
        {
            internal const String Format = "Format";
            internal const String InputLength = "InputLength";
        }
    }
}
