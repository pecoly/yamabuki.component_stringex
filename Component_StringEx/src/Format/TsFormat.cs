﻿using System;
using System.Collections.Generic;
using System.Linq;

using Yamabuki.Component.StringEx.Properties;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;

namespace Yamabuki.Component.StringEx
{
    internal class TsFormat
        : TsTask_N_1
    {
        public TsFormat(
            String definitionPath,
            IEnumerable<TsDataStoreGuid> inputDataStoreGuidList,
            TsDataStoreGuid outputDataStoreGuid)
            : base(
            definitionPath,
            inputDataStoreGuidList,
            outputDataStoreGuid)
        {
        }

        protected override void CheckInputDataList(
            IEnumerable<TsDataList> inputDataListList)
        {
        }

        protected override void CreateOutputDataList(out TsDataList outputDataList)
        {
            outputDataList = new TsDataListT<String>();
        }

        protected override void ExecuteInternal(
            IEnumerable<TsDataList> inputDataListList,
            TsDataList outputDataList)
        {
            var inputDataList0 = inputDataListList.ElementAt(0);
            var inputDataListT0 = inputDataList0 as TsDataListT<String>;

            if (inputDataListT0.Count != 1)
            {
                throw new TsInputDataInvalidLengthException(
                    1, this.DefinitionPath, FormatConstants.InputPortIndex_Format);
            }

            var format = inputDataListT0.GetValue(0);

            var outputDataListT = outputDataList as TsDataListT<String>;

            var rowLength = this.GetRowLength(inputDataListList, 1);
            var parameterLength = inputDataListList.Count() - 1;

            for (var i = 0; i < rowLength; i++)
            {
                var args = new Object[parameterLength];
                var j = 0;
                foreach (var inputDataList in inputDataListList)
                {
                    if (j > 0)
                    {
                        args[j - 1] = inputDataList.GetObjectValue(i);
                    }

                    j++;
                }

                var result = String.Format(format, args);
                outputDataListT.AddValue(result);
            }
        }

        protected Int32 GetRowLength(IEnumerable<TsDataList> inputDataListList, Int32 beginIndex)
        {
            var rowLength = inputDataListList.ElementAt(beginIndex).Count;
            for (var i = beginIndex + 1; i < inputDataListList.Count(); i++)
            {
                if (rowLength != inputDataListList.ElementAt(i).Count)
                {
                    throw new TsInputDataInvalidLengthException(
                        this.DefinitionPath,
                        Resource.InvalidParameterLength);
                }
            }

            return rowLength;
        }

        private struct FormatConstants
        {
            public static readonly Int32 InputPortIndex_Format = 0;
        }
    }
}
