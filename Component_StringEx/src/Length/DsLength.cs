﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.SimpleEditForm;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.Func;

namespace Yamabuki.Component.StringEx
{
    public class DsLength
        : DsSimpleComponent_1_1
    {
        private const String Title = "文字列長";

        internal String Description
        {
            get
            {
                return "文字列の長さを取得します。\r\n" +
                    "例 : \r\n" +
                    "入力 : あいうえお12345\r\n" +
                    "出力 : 10";
            }
        }

        public override void Initialize(IEnumerable<XElement> data)
        {
        }

        public override BaseMessage DoubleClick()
        {
            this.ShowDialog();
            return null;
        }

        protected override void Initialize_1_1()
        {
        }

        protected virtual void ShowDialog()
        {
            using (var presenter = new DsSimpleEditForm_P(this))
            {
                presenter.Show(FormSize.Auto, Title, this.Description);
            }
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuid,
            TsDataStoreGuid outputDataStoreGuid)
        {
            var func = new Func<List<String>, List<Int32>>(x =>
            {
                return x.Select(y => y == null ? 0 : y.Length).ToList();
            });

            return new TsFuncTask_1_1__N_N<String, Int32>(
                this.DefinitionPath,
                inputDataStoreGuid,
                outputDataStoreGuid,
                func);
        }
    }
}
