﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.SimpleEditForm;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.Func;

namespace Yamabuki.Component.StringEx
{
    public class DsSplit
        : DsSimpleComponent_2_1
    {
        public override String TypeName
        {
            get { return ""; }
        }

        public override int DefaultWidth
        {
            get { return 144; }
        }

        internal String Description
        {
            get
            {
                return "文字列を分割します。\r\n" +
                    "「データ」には文字列を分割したい文字列を指定してください。\r\n" +
                    "「区切り文字」には区切り文字を指定してください。\r\n" +
                    "例 : \r\n" +
                    "データ : a,b,c\r\n" +
                    "区切り文字 : ,\r\n" +
                    "出力 : [a b c]";
            }
        }

        protected override string InputName0
        {
            get { return "データ"; }
        }

        protected override string InputName1
        {
            get { return "区切り文字"; }
        }

        public override void Initialize(IEnumerable<XElement> data)
        {
        }

        public override BaseMessage DoubleClick()
        {
            this.ShowDialog();
            return null;
        }

        protected override void Initialize_2_1()
        {
        }

        protected virtual void ShowDialog()
        {
            using (var presenter = new DsSimpleEditForm_P(this))
            {
                presenter.Show(FormSize.Auto, this.TypeName, this.Description);
            }
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuid0,
            TsDataStoreGuid inputDataStoreGuid1,
            TsDataStoreGuid outputDataStoreGuid)
        {
            var func = new Func<String, String, List<String>>((target, separator) =>
                {
                    return target.Split(new String[] { separator }, StringSplitOptions.None).ToList();
                });

            return new TsFuncTask_2_1__11_N<String, String, String>(
                this.DefinitionPath,
                inputDataStoreGuid0,
                inputDataStoreGuid1,
                outputDataStoreGuid,
                func);
        }
    }
}
